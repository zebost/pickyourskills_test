from flask import Flask
from flask_rest_jsonapi import Api, ResourceDetail, ResourceList
from flask_sqlalchemy import SQLAlchemy
from marshmallow_jsonapi.flask import Schema
from marshmallow_jsonapi import fields
from sqlalchemy.orm import validates

app = Flask(__name__)
app.config['DEBUG'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///tmp/test.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)

class Skill(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)

   # @validates('name')
   # def convert_lower(self, key, value):
   #    return value.lower

class SkillSchema(Schema):
    class Meta:
        type_ = 'skill'
        self_view = 'skill_detail'
        self_view_kwargs = {'id': '<id>'}
        self_view_many = 'skill_list'

    id = fields.Integer(as_string=True, dump_only=True)
    name = fields.Str(requried=True, load_only=True)


class SkillList(ResourceList):
    schema = SkillSchema
    data_layer = {'session': db.session,
                  'model': Skill}


class SkillDetail(ResourceDetail):
    schema = SkillSchema
    data_layer = {'session': db.session,
                  'model': Skill}



api = Api(app)

api.route(SkillList, 'skill_list', '/skills')
api.route(SkillDetail, 'skill_detail', '/skills/<int:id>')
if __name__ == '__main__':
    app.run(debug=True)
